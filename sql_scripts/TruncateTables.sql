truncate table TS_Expense;
truncate table TS_Trip;
truncate table TS_User;
truncate table TS_UserTripJoin;
truncate table TS_UserExpenseJoin;

commit;
