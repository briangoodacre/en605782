/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.api;

import java.sql.SQLException;
import junit.framework.TestCase;
import org.junit.Test;

/**
 *
 * @author brian
 */
public class TripTest extends TestCase {

    public TripTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
        @Test
    public void testPass() throws SQLException {
        assertTrue(true);
    }
    
/*
    ///     // Tests methods that directly access the database
     //
     // @throws SQLException
     //
    @Test
    public void testAddExpense() throws SQLException {
        System.out.println("testAddExpense");

        Trip testTrip = TestHelper.getRandomTrip();
        Expense testExpense = TestHelper.getRandomExpense();

        // do actions
        testExpense.setTrip(testTrip);// add expense
        testExpense.saveObj();
        testTrip.saveObj(); //save
        testTrip.refreshObj();

        // read from database general
        Trip newTrip = Trip.getTrip(testTrip.getTripID());

        // test generic database reading
        assertEquals(testTrip.getTripID(), newTrip.getTripID());
        assertEquals(testTrip.getTripName(), newTrip.getTripName());
        // read from the database for expense
        assertEquals(testTrip.getExpenses().size(), newTrip.getExpenses().size());

    }
    
    @Test
    public void testAddUser() throws SQLException {
        System.out.println("testAddUser");

        // objects
        Trip testTrip = TestHelper.getRandomTrip();
        User testUser = TestHelper.getRandomUser();
        UserTrip testUserTrip = new UserTrip();

        // preconditions
        long preUserTestRowCount = testUserTrip.rowsInDatabase();

        // do actions
        testTrip.addUser(testUser);// add user
        testTrip.saveObj(); //save
        testTrip.refreshObj();

        // postconditions
        long postUserTestRowCount = testUserTrip.rowsInDatabase();
        List<User> aa = testTrip.getUsers(); // get the user

        // read from database general
        Trip newTrip = Trip.getTrip(testTrip.getTripID());

        // read from database for users
        assertEquals(testTrip.getUsers().size(), newTrip.getUsers().size());
        assertEquals(1, testTrip.getUsers().size());
        assertEquals(testUser.getUserName(), testTrip.getUsers().get(0).getUserName());
        //postconditions
        assertEquals(preUserTestRowCount + 1, postUserTestRowCount);
    }

    */
}
