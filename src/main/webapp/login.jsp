<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>TripSplit - Login</title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <header>
        <section id="banner">TripSplit</section>
    </header>
    <body>
    <section id="trip-banner">Login</section>        			    
        <form name="Login" action="login" method="post" accept-charset="utf-8">
            <% String message = request.getParameter("message");
                if (message != null) {
                    if (message.equals("invalidLogin")) {
                        out.print("Invalid login: username or password not recognized </br>");                        
                    }
                }
            %>
            <!-- method="post" -->
            <p>
            Email <input type="email" name="username" placeholder="yourname@email.com" required /> 
            Password <input type="password" name="password" placeholder="password" required />
            <input type="submit" value="Login" />
            </p>
        </form>
        <div>First Time to TripSplit? <a href="register.jsp"> Register Now! </a></div>
    </body>
</html>