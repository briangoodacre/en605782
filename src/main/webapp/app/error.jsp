<%-- 
    Document   : error
    Created on : Dec 10, 2014, 9:21:10 PM
    Author     : Kelly
--%>
<%@page isErrorPage="true"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>JSP Page</title>
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
         <%@include file="navbar.jsp"%>        
        <h1>Oops!</h1>
        <p>We're experiencing technical difficulties:<% %></p>
        <%
            String error = request.getParameter("error");
            if(null != error){
                out.print(error);
            }
            
            error = (String) session.getAttribute("errorMsg");
            if(null != error){
                out.print(error);
            }
        %>
        <input type="button" value="Return to previous page" onclick="history.back()" />
    </body>
</html>
