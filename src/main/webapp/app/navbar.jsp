<%@page import="
		tripsplit.api.User,
        tripsplit.api.Trip,
        java.util.List,
        java.text.DecimalFormat"
%>
<header>
    <section id="banner">TripSplit</section>
    <nav>
        <ul>
            <!-- Prints out the current user name -->
            <%  User curUser = (User) request.getSession().getAttribute("currentUser");
                if (null != curUser.getUserName()) {
                    out.print("<li><i>" + curUser.getUserName() + "</i></li>");
                }
                
        		DecimalFormat formatter = new DecimalFormat("$#,##0.00;-$#,##0.00");
            %>
            <li><a rel="external" href="index.jsp">Home</a></li>
            <li><a rel="external" href="create.jsp">Create Trip</a></li>
            <li><a rel="external" href="mytrips.jsp">My Trips</a><ul>
	            <%!@SuppressWarnings("unchecked")%>
	            <%
	                List<Trip> tripList = (List<Trip>) request.getSession().getAttribute("currentTrips");
	                if (tripList != null) {
	                    for (Trip currentTrip : tripList) {
	                        if (null != currentTrip.getTripName()) {
	            %>
	            				<li><a rel="external" href="trip.jsp?currentTripID=<%=currentTrip.getTripID()%>"><%=currentTrip.getTripName()%></a></li>
	            <%
	                        }
	                    }
	                }
	            %>
            </ul></li>
            <li><a rel="external" href="myexpenses.jsp">Expenses</a></li>
            <li><a rel="external" href="about.jsp">About Us</a></li>            
        </ul>

        <form name="Logout" action="../logout" accept-charset="utf-8">
            <input type="submit" value="Logout" />
        </form>
    </nav>
</header>