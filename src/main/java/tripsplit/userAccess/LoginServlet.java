package tripsplit.userAccess;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tripsplit.api.User;

/**
 * @author Seth
 *
 * The Login Servlet takes parameters from login.jsp and builds a currentUser
 * session attribute. Currently forwards user to mytrips.jsp.
 */
@WebServlet(urlPatterns={"/Login", "/login"})
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User currentUser = User.getUser(username);

        if (null == currentUser || ! password.equals(currentUser.getPassword())) {
            //if db authentication fails
            request.getRequestDispatcher("/login.jsp?message=invalidLogin").forward(request, response);
        } else {
            request.getSession().setAttribute("currentUser", currentUser);
            try {
				request.getSession().setAttribute("currentTrips", currentUser.getTrips());
			} catch (SQLException e) {
				e.printStackTrace();
			}
            response.sendRedirect("/TripSplit/app/mytrips.jsp");
        }
    }

}
