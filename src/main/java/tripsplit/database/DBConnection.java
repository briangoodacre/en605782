/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

import tripsplit.api.Expense;
import tripsplit.api.Trip;
import tripsplit.api.User;
import tripsplit.api.UserExpense;
import tripsplit.api.UserTrip;

/**
 *
 * @author brian
 */
public class DBConnection {

    // DAOs
    private static Dao<Expense, Integer> expenseDao;
    private static Dao<User, Integer> userDao;
    private static Dao<Trip, Integer> tripDao;
    private static Dao<UserTrip, Integer> userTripDao;
    private static Dao<UserExpense, Integer> userExpenseDao;

    // Logger
    private static final Logger logger = Logger.getLogger(DBConnection.class.getName());

    // connection source
    private static ConnectionSource databaseConnection;
    private static boolean isInitialized = false;

    /**
     * Attempts to prepare the multiple parts of the database for use
     *
     * @throws java.lang.Exception
     */
    private synchronized static void prepareDatabase() throws Exception {

        // get the connection to the database
        try {
            databaseConnection = getConnection();
        } catch (Exception e) {
            logger.severe("Could not get a connection to the database.");

            throw e;
        }

        // setup our database
        try {
            
            // TODO - When the tables are created, trying to create them again makes an error.
            // set manually for now
            if (DBConnectionInformation.buildDatabase)
            {
                createTables();
            }
        } catch (Exception e) {
            logger.severe("Could not get the tables are ready.");

            throw e;
        }

        // build the DAOs
        try {
            createDaos();
        } catch (Exception e) {
            logger.severe("Could not get DAOs ready.");

            throw e;
        }

        // successful
        logger.info("Successfully got the database ready.");
    }

    /**
     * Communicates with the database and gets a connection to it
     *
     * @return
     * @throws Exception
     */
    public synchronized static ConnectionSource getConnection() throws Exception {
        ConnectionSource connectionSource = null;

        // create our data-source for the database
        connectionSource = new JdbcConnectionSource
(                DBConnectionInformation.DATABASE_URL,
                DBConnectionInformation.DATABASE_USERNAME,
                DBConnectionInformation.DATABASE_PASSWORD);
        
        return connectionSource;
    }

    public synchronized static void closeConnection() throws Exception {
        // destroy the data source which should close underlying connections
        if (databaseConnection != null) {
            databaseConnection.close();
        }
    }

    /**
     * Makes sure the tables are created
     *
     * @param connectionSource
     * @throws Exception
     */
    private synchronized static void createTables() throws Exception {

        // if you need to create the table
        TableUtils.createTableIfNotExists(databaseConnection, Expense.class);
        TableUtils.createTableIfNotExists(databaseConnection, Trip.class);
        TableUtils.createTableIfNotExists(databaseConnection, UserTrip.class);
        TableUtils.createTableIfNotExists(databaseConnection, User.class);
        TableUtils.createTableIfNotExists(databaseConnection, UserExpense.class);
    }

    /**
     * Creates the DAOs
     *
     * @param connectionSource
     * @throws Exception
     */
    private synchronized static void createDaos() throws Exception {
        // Get the DAOs
        expenseDao = DaoManager.createDao(databaseConnection, Expense.class);
        userDao = DaoManager.createDao(databaseConnection, User.class);
        tripDao = DaoManager.createDao(databaseConnection, Trip.class);
        userTripDao = DaoManager.createDao(databaseConnection, UserTrip.class);
        userExpenseDao = DaoManager.createDao(databaseConnection, UserExpense.class);
    }

    /**
     * Given a class name, returns the Dao for that class
     *
     * Not scalable but good enough for now.
     *
     * @param className
     * @return
     */
    @SuppressWarnings("rawtypes")
	public static Dao getDao(String className) {
        if (!isInitialized) {
            try {
                prepareDatabase();
                isInitialized = true;
            } catch (Exception ex) {
                Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, "Could not prepare the Database completely.", ex);
                return null;
            }
        }

        // switch statement, returns the correct Dao
        switch (className) {
            case "Expense":
                return expenseDao;
            case "User":
                return userDao;
            case "UserTrip":
                return userTripDao;
            case "Trip":
                return tripDao;
            case "UserExpense":
                return userExpenseDao;
            default:
                return null;
        }
    }
}

/* sources:
 https://github.com/j256/ormlite-jdbc/blob/master/src/test/java/com/j256/ormlite/examples/simple/SimpleMain.java

 */
