/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tripsplit.api;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author brian
 */
@DatabaseTable(tableName = "TS_UserExpenseJoin")
public class UserExpense extends DaoCommon{
    // <editor-fold defaultstate="collapsed" desc="static variables">

    // column names    
    public final static String COLUMN_USER_ID = "user_id";
    public final static String COLUMN_EXPENSE_ID = "expense_id";

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Variables and database fields">
    @DatabaseField(generatedId = true)
    private int userExpenseID;

    @DatabaseField(foreign = true, index = true, columnName = COLUMN_USER_ID)
    private User user;

    @DatabaseField(foreign = true, index = true, columnName = COLUMN_EXPENSE_ID)
    private Expense expense;
    
    @DatabaseField
    private boolean collector;

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="constructor">

    public UserExpense() {

    }

    public UserExpense(User user, Expense expense, boolean collector) {
        this.user = user;
        this.expense = expense;
        this.collector = collector;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="get set variables">
    
    public int getUserExpenseID() {
        return userExpenseID;
    }

    public void setUserExpenseID(int userExpenseID) {
        this.userExpenseID = userExpenseID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    public boolean isCollector() {
        return collector;
    }

    public void setIsCollector(boolean isCollector) {
        this.collector = isCollector;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="interface method">
    @Override
    public String getClassName() {
        return UserExpense.class.getSimpleName();
    }
    
    // </editor-fold>
    
}
